"""This module defines function to load data from files"""

from pyannote.core import Segment, Annotation


def getPathToGroundtruth(episode):
    """Return path to groundtruth file for episode"""
    pathToGroundtruth = "data/tbbt_season01_annotations/" \
        + "TheBigBangTheory.Season01.Episode%02d.speakername.ctm" % episode
    return pathToGroundtruth


def get_who_speaks_when_reference(episode):
    """Return 'who speaks when' reference as pyannote.core.Annotation"""
    pathToGroundtruth = getPathToGroundtruth(episode)
    with open(pathToGroundtruth, 'r') as fp:
        who_speaks_when = Annotation()
        for line in fp:
            columns = line.strip().split()
            start = float(columns[2])
            end = start + float(columns[3])
            speaker_name = columns[7]
            who_speaks_when[Segment(start, end)] = speaker_name
    return who_speaks_when


def get_pronounced_names_reference(episode):
    """Return 'pronounced names' reference as pyannote.core.Annotation"""
    pathToGroundtruth = getPathToGroundtruth(episode)
    pronounced_names = Annotation()
    with open(pathToGroundtruth, 'r') as fp:
        for line in fp:
            columns = line.strip().split()
            start = float(columns[2])
            end = start + float(columns[3])
            name = columns[9]
            if name != '"':
                pronounced_names[Segment(start, end)] = name
    return pronounced_names


def get_speaker_diarization_reference(episode):
    """Returns 'speaker diarization' reference as pyannote.core.Annotation"""
    who_speaks_when = get_who_speaks_when_reference(episode)
    speaker_diarization = who_speaks_when
    return speaker_diarization


def getPathToAudiofile(episode):
    """Return path to groundtruth file for episode"""
    pathToAudiofile = "data/Audio/" + \
        "TheBigBangTheory.Season01.Episode%02d.en.wav" % episode
    return pathToAudiofile
