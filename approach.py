"""This module defines functions that do the actual work"""

from load import getPathToGroundtruth, get_speaker_diarization_reference
from load import get_who_speaks_when_reference, get_pronounced_names_reference
import numpy as np
from xarray import DataArray
from munkres import Munkres
from pyannote.core import Segment


def get_reference_cooccurence_list(episode):
    """Representing speaker and the name by the speaker in the below format"""

    pathToGroundtruth = getPathToGroundtruth(episode)
    previous = ''
    keys = []
    values = []
    reference_cooccurence_list = []
    pronounced_namelist = []
    speaker_namelist = []
    with open(pathToGroundtruth, 'r') as fp:
        for line in fp:
            columns = line.strip().split()
            speaker = columns[7]
            name = columns[9]
            speaker_namelist.append(speaker)
            if previous != speaker:
                keys.append(speaker)
                values.append(list())
                i = 0
            if name != '"':
                pronounced_namelist.append(name)
                values[-1].append(name)
                i += 1
            previous = speaker
        for i, speaker in enumerate(keys):
            reference_cooccurence_list.append((speaker, values[i]))
    return reference_cooccurence_list, speaker_namelist, pronounced_namelist


def get_1st_and_2nd_person_cooccurence_list(episode):
    """Representing speaker and the pronounced name by the speaker"""
    pathToGroundtruth = getPathToGroundtruth(episode)
    previous = ''
    keys = []
    values = []
    get_1st_and_2nd_person_cooccurence = []
    pronounced_namelist = []
    speaker_namelist = []
    with open(pathToGroundtruth, 'r') as fp:
        for line in fp:
            columns = line.strip().split()
            speaker = columns[7]
            name = columns[9]
            ntype = columns[10]
            speaker_namelist.append(speaker)
            if previous != speaker:
                keys.append(speaker)
                values.append(list())
                i = 0
            if ntype != '"' and ntype != 'object':
                pronounced_namelist.append(name)
                values[-1].append(name)
                i += 1
            previous = speaker
        for i, speaker in enumerate(keys):
            get_1st_and_2nd_person_cooccurence.append((speaker, values[i]))
    return (get_1st_and_2nd_person_cooccurence,
            speaker_namelist, pronounced_namelist)


def create_reference_matrix(episode):
    """Reference matrix using all the pronounced names """

    (reference_cooccurence_list,
     speaker_namelist,
     pronounced_namelist) = get_reference_cooccurence_list(episode)
    unique_speakername = sorted(np.unique(speaker_namelist))
    unique_pronouncedname = sorted(np.unique(pronounced_namelist))
    length_unique_speakername = len(unique_speakername)
    length_unique_pronouncedname = len(unique_pronouncedname)
    z = 'zzeropad {}'
    if length_unique_speakername < length_unique_pronouncedname:
        r = length_unique_pronouncedname - length_unique_speakername
        length_unique_speakername += r
        unique_speakername.extend([z.format(i) for i in range(r)])
    if length_unique_pronouncedname < length_unique_speakername:
        r = length_unique_speakername - length_unique_pronouncedname
        length_unique_pronouncedname += r
        unique_pronouncedname.extend([z.format(i) for i in range(r)])
    counts = DataArray(np.zeros((length_unique_speakername,
                                 length_unique_pronouncedname)),
                       coords=[('speaker', unique_speakername),
                               ('name', unique_pronouncedname)])
    return (counts, unique_speakername,
            unique_pronouncedname, reference_cooccurence_list)


def create_matrix_1st_and_2nd_person(episode):
    """Reference matrix using the 1st and 2nd person of person name """

    (get_1st_and_2nd_person_cooccurence,
     speaker_namelist,
     pronounced_namelist) = get_1st_and_2nd_person_cooccurence_list(episode)
    unique_speakername = sorted(np.unique(speaker_namelist))
    unique_pronouncedname = sorted(np.unique(pronounced_namelist))
    length_unique_speakername = len(unique_speakername)
    length_unique_pronouncedname = len(unique_pronouncedname)
    z = 'zzeropad {}'
    if length_unique_speakername < length_unique_pronouncedname:
        r = length_unique_pronouncedname - length_unique_speakername
        length_unique_speakername += r
        unique_speakername.extend([z.format(i) for i in range(r)])
    if length_unique_pronouncedname < length_unique_speakername:
        r = length_unique_speakername - length_unique_pronouncedname
        length_unique_pronouncedname += r
        unique_pronouncedname.extend([z.format(i) for i in range(r)])
    counts = DataArray(np.zeros((length_unique_speakername,
                                 length_unique_pronouncedname)),
                       coords=[('speaker', unique_speakername),
                               ('name', unique_pronouncedname)])
    return (counts, unique_speakername, unique_pronouncedname,
            get_1st_and_2nd_person_cooccurence)


def speech_turn_based_propagation(episode, neighbourhood):
    c1 = create_matrix_1st_and_2nd_person(episode)
    counts = c1[0]
    unique_speakername = c1[1]
    unique_pronouncedname = c1[2]
    get_1st_and_2nd_person_cooccurence = c1[3]
    cooccurence_list = get_1st_and_2nd_person_cooccurence
    counts = counts
    for i, (_, pnames) in enumerate(cooccurence_list):
        if i > 0:
            for j in range(1, neighbourhood + 1):
                P = cooccurence_list[i - j][0]
                for name in pnames:
                    counts.loc[P, name] += 1
        if i < len(cooccurence_list) - neighbourhood:
            for k in range(1, neighbourhood + 1):
                N = cooccurence_list[i + k][0]
                counts.loc[N, pnames] += 1

    propagated_names = []
    all_prop = []
    cost_matrix = np.max(counts) - counts
    m = Munkres()
    indexes = m.compute(cost_matrix)
    matched_pairs = []
    for s, n in indexes:
        if s >= len(unique_speakername) or n >= len(unique_pronouncedname):
            continue
        propagated_names.append(unique_pronouncedname[n])
        all_prop.append([unique_speakername[s], unique_pronouncedname[n]])
        who_speaks_when = get_who_speaks_when_reference(episode)
    for (x, y) in all_prop:
        if x == y:
            matched_pairs.append(x)
    who_speaks_when = get_who_speaks_when_reference(episode)
    hypothesis = who_speaks_when.subset(set(matched_pairs))
    return hypothesis


def time_based_propagation(episode, neighbourhood):

    speaker_diarization = get_speaker_diarization_reference(episode)

    pronounced_names = get_pronounced_names_reference(episode)
    Np = len(pronounced_names.labels())
    Ns = len(speaker_diarization.labels())
    name2index = {name: i for i, name in enumerate(pronounced_names.labels())}
    speaker2index = {speaker: i for i,
                     speaker in enumerate(speaker_diarization.labels())}
    N = max(Ns, Np)
    matrix = np.zeros((N, N))

    for name_segment, _, name in pronounced_names.itertracks(label=True):
        name_neighbourhood = Segment(name_segment.start - neighbourhood,
                                     name_segment.end + neighbourhood)
        name_index = name2index[name]
        n_neighbourhood = speaker_diarization.crop(name_neighbourhood)
        for speech_turn_segment, _, speaker in n_neighbourhood.itertracks(label=True):
            speaker_index = speaker2index[speaker]
            matrix[speaker_index, name_index] += speech_turn_segment.duration
            cost_matrix = np.max(matrix) - matrix

    m = Munkres()
    indexes = m.compute(cost_matrix)
    allNames = pronounced_names.labels()
    allSpeakers = speaker_diarization.labels()
    all_prop = []
    matched_pairs = []

    for s, n in indexes:
        if s >= Ns or n >= Np:
            continue
        all_prop.append([allSpeakers[s], allNames[n]])
    for (x, y) in all_prop:
        if x == y:
            matched_pairs.append(x)

    who_speaks_when = get_who_speaks_when_reference(episode)
    hypothesis = who_speaks_when.subset(set(matched_pairs))

    return hypothesis
