"""This module is an example of how you could run your experiments"""

from approach import time_based_propagation, speech_turn_based_propagation,
from load import get_who_speaks_when_reference, get_pronounced_names_reference, get_speaker_diarization_reference
from eval import evaluate_IER,

for episode in range(1, 18):

    reference = get_who_speaks_when_reference(episode)
    pronounced_names = get_pronounced_names_reference(episode)
    speaker_diarization = get_speaker_diarization_reference(episode)

    for neighborhood in [10., 20., 30.]:
        hypothesis = time_based_propagation(speaker_diarization,
                                            pronounced_names,
                                            neighborhood=neighborhood)
        performance = evaluate(reference, hypothesis)
        # you could print the performance or save it to a
        # file for #later plotting
        # you could save hypothesis to file to avoid recomputing it later,
        # using 'pyannote.core.json.dump'

    for neighborhood in [1, 2, 3]:
        hypothesis = turn_based_propagation(speaker_diarization,
                                            pronounced_names,
                                            neighborhood=neighborhood)
        performance = evaluate(reference, hypothesis)
        # ...
