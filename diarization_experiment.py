from approachspeaker import speechturn_ID_manual_K, speechturn_ID_auto_K
from approachspeaker import speechturn_ID_constrained_auto_K
from approachspeaker import speechturn_ID_constrained_manual_K
from approachspeaker import constrain_nmf_l1
from approachspeaker import calculate_speec, normal_nmf_l1
# from load import get_who_speaks_when_reference#,
# get_pronounced_names_reference
from load import get_speaker_diarization_reference
from evaluate import evaluate_DER
import scipy.io as sio
import numpy as np
# from math import sqrt
from joblib import Parallel, delayed
import multiprocessing
from datetime import datetime

data = sio.loadmat(
    'data/n_V.mat')
V = data['newV']
reference = get_speaker_diarization_reference(1)

'''
Manual Experiment
K = 10
Window size = 1.495 s
hop size = 0.046 s or 46 milliseconds
episode = 1

'''
# mkn
(v1, winil, hinil, unique_spknames,
 speaker_dict, addressee_dict, merged) = speechturn_ID_manual_K(1.495,
                                                                0.046, V, 1)
normal = {}
normal['v1'] = v1
normal['winil'] = winil
normal['hinil'] = hinil
normal['unique_spknames'] = unique_spknames
normal['speaker_dict'] = speaker_dict
normal['addressee_dict'] = addressee_dict
normal['merged'] = merged

# mkcn
(mc_v1, mc_winil, mc_hinil, mc_unique_spknames,
 mc_constrain_list, mc_speaker_dict, mc_addressee_dict,
 mc_merged) = speechturn_ID_constrained_manual_K(1.495, 0.046, V, 1)
constrained = {}
constrained['c_v1'] = mc_v1
constrained['c_winil'] = mc_winil
constrained['c_hinil'] = mc_hinil
constrained['c_constrain_list'] = mc_constrain_list
constrained['c_unique_spknames'] = mc_unique_spknames
constrained['c_addressee_dict'] = mc_addressee_dict
constrained['c_merged'] = mc_merged

sio.savemat('Results/Manual/Normal/Mnormal_test.mat', normal)
sio.savemat('Results/Manual/Improved/Mconstrained_test.mat', constrained)

# akn
(av1, awinil, ahinil, aunique_spknames,
 aspeaker_dict, aaddressee_dict, amerged) = speechturn_ID_auto_K(1.495,
                                                                 0.046, V, 1)

# akcn
(ac_v1, ac_winil, ac_hinil, ac_unique_spknames,
 ac_constrain_list, ac_speaker_dict, ac_addressee_dict,
 ac_merged) = speechturn_ID_constrained_auto_K(1.495, 0.046, V, 1)


lambdasmooth = [0.1, 0.2, 0.3]  # , 0.4, 0.5, 0.6, 0.7,
# 0.8, 0.9, 1, 2, 3, 4, 5, 10, 15, 20, 25, 30]
lamdname = ['h0_1', 'h0_2', 'h0_3', 'h0_4' 'h0_5', 'h0_6', 'h0_7', 'h0_8',
            'h0_9', 'h1', 'h2', 'h3', 'h4', 'h5', 'h10', 'h15', 'h20', 'h25',
            'h30']


def sup_do_processing(v1, winil, hinil, unique_spknames,
                      speaker_dict, addressee_dict,
                      merged, mc_v1, mc_winil, mc_hinil,
                      mc_unique_spknames,
                      mc_constrain_list, mc_speaker_dict, mc_addressee_dict,
                      mc_merged, name, A):

    # for A, name in zip(lambdasmooth, lamdname):
    startTime = datetime.now()
    print(startTime)
    print('lambda', A)
    w1_normal, h1_normal, cost_normal = normal_nmf_l1(v1, winil,
                                                      hinil, A, 10)
    np.savetxt('Results/Manual/Normal/Hmm50_h1_nor_lambda_%s.txt' %
               name, h1_normal)
    np.savetxt('Results/Manual/Normal/Hmm50_w1_nor_lambda_%s.txt' %
               name, w1_normal)
    np.savetxt('Results/Manual/Normal/Hmm50_cost_nor_lambda_%s.txt' %
               name, cost_normal)
    # print('normal_nmf done')
    c_w1, c_h1, c_cost = constrain_nmf_l1(mc_v1, mc_winil, mc_hinil, A,
                                          mc_constrain_list,
                                          mc_unique_spknames,
                                          mc_speaker_dict,
                                          mc_addressee_dict, mc_merged, 10)

    np.savetxt('Results/Manual/Improved/Hmm50_h1_cons_lambda_%s.txt' %
               name, c_h1)
    np.savetxt('Results/Manual/Improved/Hmm50_w1_cons_lambda_%s.txt' %
               name, c_w1)
    np.savetxt('Results/Manual/Improved/Hmm50_cost_cons_lambda_%s.txt' %
               name, c_cost)
    print('cons_nmf done')

    error_hyp1 = []
    error_hyp2 = []
    for i in [0.20, 0.30, 0.40]:  # , 0.50, 0.60, 0.70, 0.80, 0.90]:
        print('For threshold', i)
        hypothesis1 = calculate_speec(1.495, 0.046, h1_normal, i, 1)
        hypothesis2 = calculate_speec(1.495, 0.046, c_h1, i, 1)
        evaluate_DER1 = evaluate_DER(reference, hypothesis1)
        print("normal evaluate", evaluate_DER1)
        evaluate_DER2 = evaluate_DER(reference, hypothesis2)
        print("constrained evaluate", evaluate_DER2)
        error_hyp1.append([i, evaluate_DER1])
        error_hyp2.append([i, evaluate_DER2])
    error_hy1 = np.array(error_hyp1).T
    error_hy2 = np.array(error_hyp2).T
    print('about to save')
    np.savetxt('Results/Manual/Normal/Error_normal_HMM50_TBBT_lambda_%s.txt' %
               name, error_hy1)
    np.savetxt('Results/Manual/Improved/Error_constrained_HMM50_TBBT_lambda_%s.txt' %
               name, error_hy2)
    print(A, 'is done')
    endTime = datetime.now()
    print(endTime - startTime)
    return h1_normal


st = datetime.now()
print('starting time of parallel processing', st)
E = Parallel(n_jobs=3, verbose=10)(delayed(sup_do_processing)
                                   (v1, winil, hinil,
                                    unique_spknames,
                                    speaker_dict,
                                    addressee_dict,
                                    merged,
                                    mc_v1,
                                    mc_winil,
                                    mc_hinil,
                                    mc_unique_spknames,
                                    mc_constrain_list,
                                    mc_speaker_dict,
                                    mc_addressee_dict,
                                    mc_merged,
                                    name,
                                    A) for name, A in zip(lamdname, lambdasmooth))
en = datetime.now()
print('starting time of parallel processing', en)
print('total time', en - st)
print('E', E)
