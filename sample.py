import numpy as np
import math
import numpy.matlib
import wave
from pyannote.core import Annotation, Segment
from xarray import DataArray
from collections import defaultdict
from functools import partial
from load import get_who_speaks_when_reference, getPathToAudiofile
from load import getPathToGroundtruth


def speechturn_ID_constrained_manual_K(windowSize, step, V, episode):
    ''' Constrained intilisation for the function constained_nmf_l1 '''
    '''Function that extract the frame id's for name types speaker,addressee,
    object and returns following for NMF decomposition
    V,
    random winil(K choosen by no of unique speakers ),
    random hinil,
    unique pronounced names (with added zzilence class for noise and music),
    speaker_dict(time frame id's of speaker names )
    addresse_dict (time frame id's of addresse names ),
    object_dict (time fame id's of object names)
    pre-normalise the NMF'''
    pathToGroundtruth = getPathToGroundtruth(episode)
    numpy_empty_1d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_2d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_3d = partial(np.empty, shape=(0,), dtype=np.int32)
    speaker_dict = defaultdict(numpy_empty_1d)
    addressee_dict = defaultdict(numpy_empty_2d)
    object_dict = defaultdict(numpy_empty_3d)
    merged = defaultdict(numpy_empty_1d)
    with open(pathToGroundtruth, 'r') as fp:
        who_speaks_when = Annotation()
        s_names = Annotation()
        a_names = Annotation()
        o_names = Annotation()
        st_list = []
        en_list = []
        s_list = []
        ty_list = []
        pname_list = []
        uniqpnamelist = []
        sprevious_index = []
        snext_index = []
        aprevious_index = []
        anext_index = []
        oprevious_index = []
        onext_index = []
        speaker_st = []
        addressee_st = []
        object_st = []
        finalspeaker = []
        finaladdressee = []
        finalobject = []
        for line in fp:
            columns = line.strip().split()
            start = float(columns[2])
            end = start + float(columns[3])
            speaker_name = columns[7]
            pro_name = columns[9]
            classi = columns[10]
            who_speaks_when[Segment(start, end)] = speaker_name
            st_list.append(start)
            en_list.append(end)
            s_list.append(speaker_name)
            ty_list.append(classi)
            # print(ty_list)
            pname_list.append(pro_name)
            if pro_name != '"':
                uniqpnamelist.append(pro_name)

        ''' speaker names speaker annotation'''
        kk = [i for i, x in enumerate(ty_list) if x == 'speaker']
        for i in kk:
            sprevious_index.append(find_previous_speaker_index(s_list, i))
            snext_index.append(find_next_speaker_index(s_list, i))
        for (i, j, s) in zip(sprevious_index, snext_index, kk):
            speaker_st.append([st_list[i:j + 1], en_list[i:j + 1],
                               np.repeat(pname_list[s], len(st_list[i:j + 1]))])
        for i, z in enumerate(speaker_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalspeaker.append([st, en, pn])
        for i, line in enumerate(finalspeaker):
            start = float(line[0])
            end = float(line[1])
            p_name = line[2]
            s_names[Segment(start, end)] = p_name

        ''' Addressee names speaker annotation'''

        jj = [i for i, x in enumerate(ty_list) if x == 'addressee']
        for ii in jj:
            aprevious_index.append(find_previous_speaker_index(s_list, ii))
            anext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(aprevious_index, anext_index, kk):
            addressee_st.append([st_list[i:j + 1], en_list[i:j + 1],
                                 np.repeat(pname_list[s], len(st_list[i:j + 1]))])
        for i, z in enumerate(addressee_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finaladdressee.append([st, en, pn])
        for i, line1 in enumerate(finaladdressee):
            start = float(line1[0])
            end = float(line1[1])
            p_name = line1[2]
            a_names[Segment(start, end)] = p_name

        ''' Object names speaker annotation'''
        zz = [i for i, x in enumerate(ty_list) if x == 'object']
        for ii in zz:
            oprevious_index.append(find_previous_speaker_index(s_list, ii))
            onext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(oprevious_index, onext_index, zz):
            object_st.append([st_list[i:j + 1], en_list[i:j + 1],
                              np.repeat(pname_list[s], len(st_list[i:j + 1]))])
        for i, z in enumerate(object_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalobject.append([st, en, pn])
        for i, line2 in enumerate(finalobject):
            start1 = float(line2[0])
            end1 = float(line2[1])
            p_name1 = line2[2]
            o_names[Segment(start1, end1)] = p_name1
        # print(s_names)
        N = V.shape[1]        # No of columns of for H intilisation
        F = V.shape[0]
        unique_spknames = sorted(who_speaks_when.labels())
        Km = len(unique_spknames)
        time_series = []
        n = N + 1
        nth_ele = (step * n) + ((windowSize / 2) - step)
        n = np.arange((windowSize / 2), nth_ele, step)

        for i in n:
            time_start = i - (step / 2)
            time_end = i + (step / 2)
            time_series.append([time_start, time_end])

        ''' Speaker frame id's '''

        for i, _, j in s_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            ss = np.arange(k, r + 1, 1)
            speaker_dict[j] = np.concatenate((speaker_dict[j], ss))

        ''' Addressee frame id's '''

        for i, _, j in a_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            addressee_dict[j] = np.concatenate((addressee_dict[j],
                                                s))

        ''' Object frame id's '''

        for i, _, j in o_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            object_dict[j] = np.concatenate(
                (object_dict[j], s))  # object frame extraction

        '''Intersetion of adressee and object frames id's'''

        intersection = set(addressee_dict.keys()) & set(object_dict.keys())
        for key_merged in intersection:
            merged[key_merged] = np.union1d(
                addressee_dict[key_merged], object_dict[key_merged])
        unique_spknames.append('zzilence')

        '''Generate random winil and hnil'''

        h1 = np.abs(np.random.rand(Km + 1, N))
        w1 = np.abs(np.random.rand(F, Km + 1))
        frame_id = np.arange(0, N)
        scale_1 = np.sum(w1, axis=0)
        winil = np.multiply(w1, np.matlib.repmat((1 / scale_1), F, 1))
        hinil = np.multiply(h1, np.matlib.repmat(
            (scale_1[:, np.newaxis]), 1, N))
        H_st_constraint = DataArray(hinil,
                                    coords=[('pronames', unique_spknames),
                                            ('framenumber',
                                             frame_id)])
        '''Replace the speaker columns by 0 except the speaker name '''
        constrain_list = []
        for key, values in speaker_dict.items():
            for value in values:
                constrain_list.append(value)
                H_st_constraint.loc[:, value] = 0
                H_st_constraint.loc[key, value] = 1

        return (V, winil, H_st_constraint.data, unique_spknames, constrain_list, addressee_dict,
                merged)
