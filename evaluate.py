"""This module defines function to evaluate performance"""

from pyannote.metrics.identification import IdentificationErrorRate
from pyannote.metrics.diarization import DiarizationErrorRate


def evaluate_IER(reference, hypothesis):
    """Evaluate hypothesis provided as pyannote.core.Annotation"""
    identificationErrorRate = IdentificationErrorRate()
    return identificationErrorRate(reference, hypothesis)


def evaluate_DER(reference, hypothesis):
    ''' Evaluate Speaker Diarization using Pyannote.metrics.diarization'''
    metric = DiarizationErrorRate()
    DER = metric(reference, hypothesis, detailed=True)
    return DER.get('diarization error rate')
