"""This module defines functions that do speaker diarization approach"""
import numpy as np
import math
import numpy.matlib
import wave
from pyannote.core import Annotation, Segment
from xarray import DataArray
from collections import defaultdict
from functools import partial
from load import get_who_speaks_when_reference, getPathToAudiofile
from load import getPathToGroundtruth


def getduration(episode):
    ''' get duration of the audio of episode '''
    pathToAudioFile = getPathToAudiofile(episode)
    with contextlib.closing(wave.open(pathToAudioFile, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
    return duration


def find_next_speaker_index(s_list, ns):
    '''find the speaker ending time from a  pronounced name'''
    i = 0
    while(True):
        i += 1

        if ns + i >= len(s_list):
            return 0
        if s_list[ns] == s_list[ns + i]:
            pass
        else:
            return ns + (i - 1)


def find_previous_speaker_index(s_list, ns):
    '''Find the speaker starting label from a pronounced name'''
    i = 0
    while(True):
        i = i - 1
        if ns - i < 0:
            return ns
        if s_list[ns] == s_list[ns + i]:
            pass
        else:
            return ns + (i + 1)


def get_window(states, windowSize, step):

    Matrix = []

    K = [states[i: i + windowSize] for i in range(0, len(states), step)]

    for i in K:
        if len(i) >= (windowSize - 1):

            Matrix.append(i)
    return K


def get_V_matrix(s_t, Matrix):
    '''Generate V matrix by counting the no of states'''
    E = np.finfo(float).eps

    V = np.zeros((s_t, len(Matrix)))

    for i, row in enumerate(Matrix):

        a = np.bincount(row)

        b = np.zeros(s_t)

        b[:len(a)] = a

        V[:, i] = b

    V[V == 0] = E

    return V


def get_kmeans(data, n_states):
    ''' K - means to esitmate the closest centroid '''
    k_means = KMeans(n_clusters=n_states, verbose=True, max_iter=300, n_init=5)
    k_means.fit(data.T)
    states = k_means.predict(data.T)
    return states


def calculate_speec(windowSize, step, matrix, threshold, episode):
    who_speaks_when = get_who_speaks_when_reference(episode)
    time_series = []
    speaker_results1 = []
    n = matrix.shape[1] + 1
    nth_ele = (step * n) + ((windowSize / 2) - step)
    n = np.arange((windowSize / 2), nth_ele, step)
    for i in n:
        time_start = i - (step / 2)
        time_end = i + (step / 2)
        time_series.append([time_start, time_end])
    for s, i in enumerate(matrix):
        for e, j in enumerate(i):
            if j >= threshold:
                speaker_results1.append(
                    [time_series[e][0], time_series[e][1], j, s])
    nmf_speaks = Annotation()
    speech_nmf_speaks = Annotation()
    for i, line in enumerate(speaker_results1):
        start = float(line[0])
        end = float(line[1])
        speaker_name = float(line[3])
        nmf_speaks[Segment(start, end)] = speaker_name
    speech_nmf_speaks = Annotation()
    fs = []
    fe = []
    labl = []
    for segmen in who_speaks_when.itersegments():
        k = nmf_speaks.crop(segmen)
        for i, j, l in k.itertracks(label=True):
            fs.append(i.start)
            fe.append(i.end)
            labl.append(l)
    for a, b, c in zip(fs, fe, labl):
        star1 = float(a)
        end1 = float(b)
        spekr = c
        speech_nmf_speaks[Segment(star1, end1)] = spekr
    return speech_nmf_speaks


def normal_nmf_l1(v1, winil, hinil, lambdasmooth, niter=None):
    '''NMF decomposition in python without'''
    n = hinil.shape[1]
    f = v1.shape[0]
    scale_1 = np.sum(winil, axis=0)
    w1 = np.multiply(winil, np.matlib.repmat((1 / scale_1), f, 1))
    h1 = np.multiply(hinil, np.matlib.repmat((scale_1[:, np.newaxis]), 1, n))
    if niter is not None:
        cost = np.zeros(niter)
        cost[0] = calc_cout(v1, w1, h1, lambdasmooth)
        keep_score = True
    for i in range(1, niter):
        ratio = np.divide(v1, np.dot(w1, h1))
        inds = np.where(np.isnan(ratio))
        ratio[inds] = 1
        psi_k_n = np.multiply(h1, (np.dot((w1.T), ratio)))
        for j in range(0, n):
            psi_k = psi_k_n[:, j]  # returns array of first colum
            h_n_prech = h1[:, max(0, j - 1)]
            h_n_suiv = h1[:, min(n - 1, j + 1)]
            h_min = np.minimum(h_n_prech, h_n_suiv)
            h_max = np.maximum(h_n_prech, h_n_suiv)
            val_p1l = 1 - (2 * lambdasmooth) - np.divide(psi_k, h_min)
            val_p1r = val_p1l + (2 * lambdasmooth)
            val_p2l = 1 - np.divide(psi_k, h_max)
            val_p2r = val_p2l + 2 * lambdasmooth
            force_h_min = 1 * \
                (numpy.logical_and((val_p1l <= 0), (val_p1r >= 0)))
            force_h_max = 1 * \
                (numpy.logical_and((val_p2l <= 0), (val_p2r >= 0)))
            # np.asarray(jj).reshape((jj.shape[0]))
            h1[:, j] = np.divide(
                psi_k, (1 + 2 * lambdasmooth * (1 * (val_p2r < 0) - 1 * (val_p1l > 0))))
            h1[np.where(force_h_min)[0], np.array(j)] = h_min[
                np.where(force_h_min)]  # look closely
            h1[np.where(force_h_max)[0], np.array(j)] = h_max[
                np.where(force_h_max)]  # look closely
        ratio = np.divide(v1, np.dot(w1, h1))
        inds = np.where(np.isnan(ratio))
        ratio[inds] = 1
        phi_f_k = np.multiply(w1, np.dot(ratio, h1.T))
        w1 = (phi_f_k) / ((np.sum(h1, axis=1)).T + lambdasmooth *
                          (np.sum((abs(h1[:, 1:] - h1[:, :-1])), axis=1)).T)
        # renormalisation
        scale_1 = np.sum(w1, axis=0)
        wtile_size = w1.shape[0]
        w1 = np.multiply(w1, np.tile((1 / scale_1), (wtile_size, 1)))
        htile_size = h1.shape[1]
        h1 = np.multiply(h1, np.tile(scale_1[:, np.newaxis], (1, htile_size)))
        if keep_score is True:
            cost[i] = calc_cout(v1, w1, h1, lambdasmooth)
    max_h1 = h1.max(axis=0)
    h1 = h1 / max_h1
    return w1, h1, cost


def constrain_nmf_l1(v1, winil, hinil, lambdasmooth, constrain_list, unique_pronames, speaker_dict, addressee_dict, object_dict, niter):
    ''' NMF decomposition in python without'''
    ''' Use speechturn_ID_constrained_auto_K,
        speechturn_ID_constrained_Manual_K'''
    n = hinil.shape[1]
    f = v1.shape[0]
    scale_1 = np.sum(winil, axis=0)
    w1 = np.multiply(winil, np.matlib.repmat((1 / scale_1), f, 1))
    h1 = np.multiply(hinil, np.matlib.repmat((scale_1[:, np.newaxis]), 1, n))
    frame_id = np.arange(0, n)
    unique_pronames = sorted(unique_pronames)
    H_st_constraint = DataArray(h1, coords=[('pronames', unique_pronames),
                                            ('framenumber', frame_id)])
    for key, values in speaker_dict.items():
        for value in values:
            constrain_list.append(value)
            H_st_constraint.loc[:, value] = 0
            H_st_constraint.loc[key, value] = 1
    h1 = H_st_constraint.data
    if niter is not None:
        cost = np.zeros(niter)
        cost[0] = calc_cout(v1, w1, h1, lambdasmooth)
        keep_score = True
    for i in range(1, niter):
        print(i)
        ratio = np.divide(v1, np.dot(w1, h1))
        inds = np.where(np.isnan(ratio))
        ratio[inds] = 1
        psi_k_n = np.multiply(h1, (np.dot((w1.T), ratio)))
        for j in range(0, n):
            if j not in constrain_list:
                psi_k = psi_k_n[:, j]  # returns array of first colum
                h_n_prech = h1[:, max(0, j - 1)]
                h_n_suiv = h1[:, min(n - 1, j + 1)]
                h_min = np.minimum(h_n_prech, h_n_suiv)
                h_max = np.maximum(h_n_prech, h_n_suiv)
                val_p1l = 1 - (2 * lambdasmooth) - np.divide(psi_k, h_min)
                val_p1r = val_p1l + (2 * lambdasmooth)
                val_p2l = 1 - np.divide(psi_k, h_max)
                val_p2r = val_p2l + 2 * lambdasmooth
                force_h_min = 1 * \
                    (numpy.logical_and((val_p1l <= 0), (val_p1r >= 0)))
                force_h_max = 1 * \
                    (numpy.logical_and((val_p2l <= 0), (val_p2r >= 0)))
                h1[:, j] = np.divide(
                    psi_k, (1 + 2 * lambdasmooth * (1 * (val_p2r < 0) - 1 * (val_p1l > 0))))
                h1[np.where(force_h_min)[0], np.array(j)] = h_min[
                    np.where(force_h_min)]  # look closely
                h1[np.where(force_h_max)[0], np.array(j)] = h_max[
                    np.where(force_h_max)]  # look closely
        ratio = np.divide(v1, np.dot(w1, h1))
        inds = np.where(np.isnan(ratio))
        ratio[inds] = 1
        phi_f_k = np.multiply(w1, np.dot(ratio, h1.T))
        w1 = (phi_f_k) / ((np.sum(h1, axis=1)).T + lambdasmooth *
                          (np.sum((abs(h1[:, 1:] - h1[:, :-1])), axis=1)).T)
        # renormalisation
        scale_1 = np.sum(w1, axis=0)
        wtile_size = w1.shape[0]
        w1 = np.multiply(w1, np.tile((1 / scale_1), (wtile_size, 1)))
        htile_size = h1.shape[1]
        h1 = np.multiply(h1, np.tile(scale_1[:, np.newaxis], (1, htile_size)))
        if keep_score is True:
            cost[i] = calc_cout(v1, w1, h1, lambdasmooth)
    max_h1 = h1.max(axis=0)
    h1 = h1 / max_h1
    max_h1 = h1.max(axis=0)
    h1 = h1 / max_h1
    frame_id = np.arange(0, n)
    unique_pronames = sorted(unique_pronames)
    H_st_constraint1 = DataArray(h1, coords=[('pronames', unique_pronames),
                                             ('framenumber', frame_id)])

    '''Replace with addressee name frames with 0 '''
    # print(addressee_dict)
    for key, values in addressee_dict.items():
        for value in values:
            H_st_constraint1.loc[key, value] = 0
            H_st_constraint1.loc['zzilence', value] = 0
            # print(H_st_constraint.loc[key, value])
    '''Replace with addressee name frames with 0 '''
    for key, values in object_dict.items():
        for value in values:
            H_st_constraint1.loc[key, value] = 0
            H_st_constraint1.loc['zzilence', value] = 0

    return w1, H_st_constraint1.data, cost


def calc_cout(v1, w1, h1, lambdasmooth):
    '''Cost calculation '''
    v_app1 = np.dot(w1, h1)
    z = np.divide(v1, v_app1)
    v_tmpl = np.multiply(v1, np.log(z))
    v_tmpl = np.nan_to_num(v_tmpl)
    v_tmpl = v_tmpl - v1 + v_app1
    v_tempj = abs(h1[:, 1:] - h1[:, :-1])  # abs(h1[1:] - h1[:-1])
    cout = v_tmpl.sum() + lambdasmooth * (v_tempj.sum())
    return cout


def speechturn_ID_auto_K(windowSize, step, V, episode):
    '''function that extract the frame id's for name types speaker, addressee,
    object and returns following to do NMF decomposition
    V,
    random winil(K choosen by no unique names),
    random hinil,
    unique pronounced names(with added zzilence class for noise and music),
    speaker_dict(time frame id's of speaker names)
    addresse_dict(time frame id's of addresse names),
    object_dict (time fame id's of object names)'''

    pathToGroundtruth = getPathToGroundtruth(episode)
    numpy_empty_d1 = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_d2 = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_d3 = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_d4 = partial(np.empty, shape=(0,), dtype=np.int32)

    speaker_dict = defaultdict(numpy_empty_d1)
    addressee_dict = defaultdict(numpy_empty_d2)
    object_dict = defaultdict(numpy_empty_d3)
    merged = defaultdict(numpy_empty_d4)
    with open(pathToGroundtruth, 'r') as fp:
        who_speaks_when = Annotation()
        s_names = Annotation()
        a_names = Annotation()
        o_names = Annotation()
        st_list = []
        en_list = []
        s_list = []
        ty_list = []
        pname_list = []
        uniqpnamelist = []
        sprevious_index = []
        snext_index = []
        aprevious_index = []
        anext_index = []
        oprevious_index = []
        onext_index = []
        speaker_st = []
        addressee_st = []
        object_st = []
        finalspeaker = []
        finaladdressee = []
        finalobject = []
        for line in fp:
            columns = line.strip().split()
            start = float(columns[2])
            end = start + float(columns[3])
            speaker_name = columns[7]
            pro_name = columns[9]
            classi = columns[10]
            who_speaks_when[Segment(start, end)] = speaker_name
            st_list.append(start)
            en_list.append(end)
            s_list.append(speaker_name)
            ty_list.append(classi)
            # print(ty_list)
            pname_list.append(pro_name)
            if pro_name != '"':
                uniqpnamelist.append(pro_name)

        ''' speaker names speaker annotation'''
        kk = [i for i, x in enumerate(ty_list) if x == 'speaker']
        for i in kk:
            sprevious_index.append(find_previous_speaker_index(s_list, i))
            snext_index.append(find_next_speaker_index(s_list, i))
        for (i, j, s) in zip(sprevious_index, snext_index, kk):
            speaker_st.append([st_list[i:j + 1], en_list[i:j + 1],
                               np.repeat(pname_list[s],
                                         len(st_list[i:j + 1]))])
        for i, z in enumerate(speaker_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalspeaker.append([st, en, pn])
        for i, line in enumerate(finalspeaker):
            start = float(line[0])
            end = float(line[1])
            p_name = line[2]
            s_names[Segment(start, end)] = p_name

        ''' Addressee names speaker annotation'''

        jj = [i for i, x in enumerate(ty_list) if x == 'addressee']
        for ii in jj:
            aprevious_index.append(find_previous_speaker_index(s_list, ii))
            anext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(aprevious_index, anext_index, kk):
            addressee_st.append([st_list[i:j + 1], en_list[i:j + 1],
                                 np.repeat(pname_list[s],
                                           len(st_list[i:j + 1]))])
        for i, z in enumerate(addressee_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finaladdressee.append([st, en, pn])
        for i, line1 in enumerate(finaladdressee):
            start = float(line1[0])
            end = float(line1[1])
            p_name = line1[2]
            a_names[Segment(start, end)] = p_name

        ''' Object names speaker annotation'''
        zz = [i for i, x in enumerate(ty_list) if x == 'object']
        for ii in zz:
            oprevious_index.append(find_previous_speaker_index(s_list, ii))
            onext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(oprevious_index, onext_index, zz):
            object_st.append([st_list[i:j + 1], en_list[i:j + 1],
                              np.repeat(pname_list[s], len(st_list[i:j + 1]))])
        for i, z in enumerate(object_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalobject.append([st, en, pn])
        for i, line2 in enumerate(finalobject):
            start1 = float(line2[0])
            end1 = float(line2[1])
            p_name1 = line2[2]
            o_names[Segment(start1, end1)] = p_name1
        # print(s_names)
        N = V.shape[1]        # No of columns of for H intilisation
        F = V.shape[0]
        unique_pronames = sorted(np.unique(uniqpnamelist))
        Km = len(np.unique(uniqpnamelist))
        time_series = []
        n = N + 1
        nth_ele = (step * n) + ((windowSize / 2) - step)
        n = np.arange((windowSize / 2), nth_ele, step)
        for i in n:
            time_start = i - (step / 2)
            time_end = i + (step / 2)
            time_series.append([time_start, time_end])
        ''' Speaker frame id's '''
        for i, _, j in s_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            ss = np.arange(k, r + 1, 1)
            speaker_dict[j] = np.concatenate((speaker_dict[j], ss))
        ''' Addressee frame id's '''
        for i, _, j in a_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            addressee_dict[j] = np.concatenate(
                (addressee_dict[j], s))  # addressee frame extraction
        ''' Object frame id's '''
        for i, _, j in o_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            object_dict[j] = np.concatenate(
                (object_dict[j], s))  # object frame extraction
        '''Intersetion of adressee and object frames id's'''
        intersection = set(addressee_dict.keys()) & set(object_dict.keys())
        for key_merged in intersection:
            merged[key_merged] = np.union1d(
                addressee_dict[key_merged], object_dict[key_merged])
        unique_pronames.append('zzilence')
        '''Generate random winil and hnil'''
        hinil = np.abs(np.random.rand(Km + 1, N))
        winil = np.abs(np.random.rand(F, Km + 1))
    return (V, winil, hinil, unique_pronames, speaker_dict, addressee_dict, merged)


def speechturn_ID_manual_K(windowSize, step, V, episode):
    '''function that extract the frame id's for name types speaker,addressee,
    object and returns following to do NMF decomposition
    V,
    random winil(K choosen by no speakers ),
    random hinil,
    unique pronounced names (with added zzilence class for noise and music),
    speaker_dict(time frame id's of speaker names )
    addresse_dict (time frame id's of addresse names ),
    object_dict (time fame id's of object names)'''

    pathToGroundtruth = getPathToGroundtruth(episode)
    numpy_empty_d1 = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_d2 = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_d3 = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_d4 = partial(np.empty, shape=(0,), dtype=np.int32)
    speaker_dict = defaultdict(numpy_empty_d1)
    addressee_dict = defaultdict(numpy_empty_d2)
    object_dict = defaultdict(numpy_empty_d3)
    merged = defaultdict(numpy_empty_d4)
    with open(pathToGroundtruth, 'r') as fp:
        who_speaks_when = Annotation()
        s_names = Annotation()
        a_names = Annotation()
        o_names = Annotation()
        st_list = []
        en_list = []
        s_list = []
        ty_list = []
        pname_list = []
        uniqpnamelist = []
        sprevious_index = []
        snext_index = []
        aprevious_index = []
        anext_index = []
        oprevious_index = []
        onext_index = []
        speaker_st = []
        addressee_st = []
        object_st = []
        finalspeaker = []
        finaladdressee = []
        finalobject = []
        for line in fp:
            columns = line.strip().split()
            start = float(columns[2])
            end = start + float(columns[3])
            speaker_name = columns[7]
            pro_name = columns[9]
            classi = columns[10]
            who_speaks_when[Segment(start, end)] = speaker_name
            st_list.append(start)
            en_list.append(end)
            s_list.append(speaker_name)
            ty_list.append(classi)
            # print(ty_list)
            pname_list.append(pro_name)
            if pro_name != '"':
                uniqpnamelist.append(pro_name)

        ''' speaker names speaker annotation'''
        kk = [i for i, x in enumerate(ty_list) if x == 'speaker']
        for i in kk:
            sprevious_index.append(find_previous_speaker_index(s_list, i))
            snext_index.append(find_next_speaker_index(s_list, i))
        for (i, j, s) in zip(sprevious_index, snext_index, kk):
            speaker_st.append([st_list[i:j + 1], en_list[i:j + 1],
                               np.repeat(pname_list[s],
                                         len(st_list[i:j + 1]))])
        for i, z in enumerate(speaker_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalspeaker.append([st, en, pn])
        for i, line in enumerate(finalspeaker):
            start = float(line[0])
            end = float(line[1])
            p_name = line[2]
            s_names[Segment(start, end)] = p_name

        ''' Addressee names speaker annotation'''

        jj = [i for i, x in enumerate(ty_list) if x == 'addressee']
        for ii in jj:
            aprevious_index.append(find_previous_speaker_index(s_list, ii))
            anext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(aprevious_index, anext_index, kk):
            addressee_st.append([st_list[i:j + 1], en_list[i:j + 1],
                                 np.repeat(pname_list[s],
                                           len(st_list[i:j + 1]))])
        for i, z in enumerate(addressee_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finaladdressee.append([st, en, pn])
        for i, line1 in enumerate(finaladdressee):
            start = float(line1[0])
            end = float(line1[1])
            p_name = line1[2]
            a_names[Segment(start, end)] = p_name

        ''' Object names speaker annotation'''
        zz = [i for i, x in enumerate(ty_list) if x == 'object']
        for ii in zz:
            oprevious_index.append(find_previous_speaker_index(s_list, ii))
            onext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(oprevious_index, onext_index, zz):
            object_st.append([st_list[i:j + 1], en_list[i:j + 1],
                              np.repeat(pname_list[s], len(st_list[i:j + 1]))])
        for i, z in enumerate(object_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalobject.append([st, en, pn])
        for i, line2 in enumerate(finalobject):
            start1 = float(line2[0])
            end1 = float(line2[1])
            p_name1 = line2[2]
            o_names[Segment(start1, end1)] = p_name1
        # print(s_names)
        N = V.shape[1]        # No of columns of for H intilisation
        F = V.shape[0]
        unique_spknames = sorted(who_speaks_when.labels())
        Km = len(np.unique(unique_spknames))
        time_series = []
        n = N + 1
        nth_ele = (step * n) + ((windowSize / 2) - step)
        n = np.arange((windowSize / 2), nth_ele, step)
        for i in n:
            time_start = i - (step / 2)
            time_end = i + (step / 2)
            time_series.append([time_start, time_end])
        ''' Speaker frame id's '''
        for i, _, j in s_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            ss = np.arange(k, r + 1, 1)
            speaker_dict[j] = np.concatenate((speaker_dict[j], ss))
        ''' Addressee frame id's '''
        for i, _, j in a_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            addressee_dict[j] = np.concatenate(
                (addressee_dict[j], s))  # addressee frame extraction
        ''' Object frame id's '''
        for i, _, j in o_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            object_dict[j] = np.concatenate(
                (object_dict[j], s))  # object frame extraction
        '''Intersetion of adressee and object frames id's'''
        intersection = set(addressee_dict.keys()) & set(object_dict.keys())
        for key_merged in intersection:
            merged[key_merged] = np.union1d(
                addressee_dict[key_merged], object_dict[key_merged])
        unique_spknames.append('zzilence')
        '''Generate random winil and hnil'''
        hinil = np.abs(np.random.rand(Km + 1, N))
        winil = np.abs(np.random.rand(F, Km + 1))
    return (V, winil, hinil, unique_spknames, speaker_dict,
            addressee_dict, merged)


def speechturn_ID_constrained_auto_K(windowSize, step, V, episode):
    ''' Constrained intilisation for the function constained_nmf_l1 '''
    '''Function that extract the frame id's for name types speaker,addressee,
    object and returns following to do NMF decomposition
    V,
    random winil(K choosen by no unique names ),
    random hinil,
    unique pronounced names (with added zzilence class for noise and music),
    speaker_dict(time frame id's of speaker names )
    addresse_dict (time frame id's of addresse names ),
    object_dict (time fame id's of object names)'''
    pathToGroundtruth = getPathToGroundtruth(episode)
    numpy_empty_1d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_2d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_3d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_4d = partial(np.empty, shape=(0,), dtype=np.int32)

    speaker_dict = defaultdict(numpy_empty_1d)
    addressee_dict = defaultdict(numpy_empty_2d)
    object_dict = defaultdict(numpy_empty_3d)
    merged = defaultdict(numpy_empty_4d)
    with open(pathToGroundtruth, 'r') as fp:
        who_speaks_when = Annotation()
        s_names = Annotation()
        a_names = Annotation()
        o_names = Annotation()
        st_list = []
        en_list = []
        s_list = []
        ty_list = []
        pname_list = []
        uniqpnamelist = []
        sprevious_index = []
        snext_index = []
        aprevious_index = []
        anext_index = []
        oprevious_index = []
        onext_index = []
        speaker_st = []
        addressee_st = []
        object_st = []
        finalspeaker = []
        finaladdressee = []
        finalobject = []
        for line in fp:
            columns = line.strip().split()
            start = float(columns[2])
            end = start + float(columns[3])
            speaker_name = columns[7]
            pro_name = columns[9]
            classi = columns[10]
            who_speaks_when[Segment(start, end)] = speaker_name
            st_list.append(start)
            en_list.append(end)
            s_list.append(speaker_name)
            ty_list.append(classi)
            # print(ty_list)
            pname_list.append(pro_name)
            if pro_name != '"':
                uniqpnamelist.append(pro_name)

        ''' speaker names speaker annotation'''
        kk = [i for i, x in enumerate(ty_list) if x == 'speaker']
        for i in kk:
            sprevious_index.append(find_previous_speaker_index(s_list, i))
            snext_index.append(find_next_speaker_index(s_list, i))
        for (i, j, s) in zip(sprevious_index, snext_index, kk):
            speaker_st.append([st_list[i:j + 1], en_list[i:j + 1],
                               np.repeat(pname_list[s],
                                         len(st_list[i:j + 1]))])
        for i, z in enumerate(speaker_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalspeaker.append([st, en, pn])
        for i, line in enumerate(finalspeaker):
            start = float(line[0])
            end = float(line[1])
            p_name = line[2]
            s_names[Segment(start, end)] = p_name

        ''' Addressee names speaker annotation'''

        jj = [i for i, x in enumerate(ty_list) if x == 'addressee']
        for ii in jj:
            aprevious_index.append(find_previous_speaker_index(s_list, ii))
            anext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(aprevious_index, anext_index, kk):
            addressee_st.append([st_list[i:j + 1], en_list[i:j + 1],
                                 np.repeat(pname_list[s],
                                           len(st_list[i:j + 1]))])
        for i, z in enumerate(addressee_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finaladdressee.append([st, en, pn])
        for i, line1 in enumerate(finaladdressee):
            start = float(line1[0])
            end = float(line1[1])
            p_name = line1[2]
            a_names[Segment(start, end)] = p_name

        ''' Object names speaker annotation'''
        zz = [i for i, x in enumerate(ty_list) if x == 'object']
        for ii in zz:
            oprevious_index.append(find_previous_speaker_index(s_list, ii))
            onext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(oprevious_index, onext_index, zz):
            object_st.append([st_list[i:j + 1], en_list[i:j + 1],
                              np.repeat(pname_list[s], len(st_list[i:j + 1]))])
        for i, z in enumerate(object_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalobject.append([st, en, pn])
        for i, line2 in enumerate(finalobject):
            start1 = float(line2[0])
            end1 = float(line2[1])
            p_name1 = line2[2]
            o_names[Segment(start1, end1)] = p_name1
        # print(s_names)
        N = V.shape[1]        # No of columns of for H intilisation
        F = V.shape[0]
        unique_pronames = sorted(np.unique(uniqpnamelist))
        Km = len(np.unique(uniqpnamelist))
        time_series = []
        n = N + 1
        nth_ele = (step * n) + ((windowSize / 2) - step)
        n = np.arange((windowSize / 2), nth_ele, step)

        for i in n:
            time_start = i - (step / 2)
            time_end = i + (step / 2)
            time_series.append([time_start, time_end])

        ''' Speaker frame id's '''

        for i, _, j in s_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            ss = np.arange(k, r + 1, 1)
            speaker_dict[j] = np.concatenate((speaker_dict[j], ss))

        ''' Addressee frame id's '''

        for i, _, j in a_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            addressee_dict[j] = np.concatenate(
                (addressee_dict[j], s))  # addressee frame extraction

        ''' Object frame id's '''

        for i, _, j in o_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            object_dict[j] = np.concatenate(
                (object_dict[j], s))  # object frame extraction

        '''Intersetion of adressee and object frames id's'''

        intersection = set(addressee_dict.keys()) & set(object_dict.keys())
        for key_merged in intersection:
            merged[key_merged] = np.union1d(
                addressee_dict[key_merged], object_dict[key_merged])
        unique_pronames.append('zzilence')

        '''Generate random winil and hnil'''

        h1 = np.abs(np.random.rand(Km + 1, N))
        w1 = np.abs(np.random.rand(F, Km + 1))
        frame_id = np.arange(0, N)
        scale_1 = np.sum(w1, axis=0)
        winil = np.multiply(w1, np.matlib.repmat((1 / scale_1), F, 1))
        hinil = np.multiply(h1, np.matlib.repmat(
            (scale_1[:, np.newaxis]), 1, N))
        H_st_constraint = DataArray(hinil,
                                    coords=[('pronames', unique_pronames),
                                            ('framenumber',
                                             frame_id)])
        '''Replace the speaker columns by 0 except the speaker name '''
        constrain_list = []
        for key, values in speaker_dict.items():
            for value in values:
                constrain_list.append(value)
                H_st_constraint.loc[:, value] = 0
                H_st_constraint.loc[key, value] = 1

        return (V, winil, hinil, unique_pronames, constrain_list, speaker_dict,
                addressee_dict, object_dict)


def speechturn_ID_constrained_manual_K(windowSize, step, V, episode):
    ''' Constrained intilisation for the function constained_nmf_l1 '''
    '''Function that extract the frame id's for name types speaker,addressee,
    object and returns following to do NMF decomposition
    V,
    random winil(K choosen by no of unique speakers ),
    random hinil,
    unique pronounced names (with added zzilence class for noise and music),
    speaker_dict(time frame id's of speaker names )
    addresse_dict (time frame id's of addresse names ),
    object_dict (time fame id's of object names)
    pre-normalise the NMF'''
    pathToGroundtruth = getPathToGroundtruth(episode)
    numpy_empty_1d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_2d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_3d = partial(np.empty, shape=(0,), dtype=np.int32)
    numpy_empty_4d = partial(np.empty, shape=(0,), dtype=np.int32)
    speaker_dict = defaultdict(numpy_empty_1d)
    addressee_dict = defaultdict(numpy_empty_2d)
    object_dict = defaultdict(numpy_empty_3d)
    merged = defaultdict(numpy_empty_4d)
    with open(pathToGroundtruth, 'r') as fp:
        who_speaks_when = Annotation()
        s_names = Annotation()
        a_names = Annotation()
        o_names = Annotation()
        st_list = []
        en_list = []
        s_list = []
        ty_list = []
        pname_list = []
        uniqpnamelist = []
        sprevious_index = []
        snext_index = []
        aprevious_index = []
        anext_index = []
        oprevious_index = []
        onext_index = []
        speaker_st = []
        addressee_st = []
        object_st = []
        finalspeaker = []
        finaladdressee = []
        finalobject = []
        for line in fp:
            columns = line.strip().split()
            start = float(columns[2])
            end = start + float(columns[3])
            speaker_name = columns[7]
            pro_name = columns[9]
            classi = columns[10]
            who_speaks_when[Segment(start, end)] = speaker_name
            st_list.append(start)
            en_list.append(end)
            s_list.append(speaker_name)
            ty_list.append(classi)
            # print(ty_list)
            pname_list.append(pro_name)
            if pro_name != '"':
                uniqpnamelist.append(pro_name)
        ''' speaker names speaker annotation'''

        kk = [i for i, x in enumerate(ty_list) if x == 'speaker']
        for i in kk:
            sprevious_index.append(find_previous_speaker_index(s_list, i))
            snext_index.append(find_next_speaker_index(s_list, i))
        for (i, j, s) in zip(sprevious_index, snext_index, kk):
            speaker_st.append([st_list[i:j + 1], en_list[i:j + 1],
                               np.repeat(pname_list[s],
                                         len(st_list[i:j + 1]))])
        for i, z in enumerate(speaker_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalspeaker.append([st, en, pn])
        for i, line in enumerate(finalspeaker):
            start = float(line[0])
            end = float(line[1])
            p_name = line[2]
            s_names[Segment(start, end)] = p_name

        ''' Addressee names speaker annotation'''
        jj = [i for i, x in enumerate(ty_list) if x == 'addressee']
        for ii in jj:
            aprevious_index.append(find_previous_speaker_index(s_list, ii))
            anext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(aprevious_index, anext_index, kk):
            addressee_st.append([st_list[i:j + 1], en_list[i:j + 1],
                                 np.repeat(pname_list[s],
                                           len(st_list[i:j + 1]))])
        for i, z in enumerate(addressee_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finaladdressee.append([st, en, pn])
        for i, line1 in enumerate(finaladdressee):
            start = float(line1[0])
            end = float(line1[1])
            p_name = line1[2]
            a_names[Segment(start, end)] = p_name

        ''' Object names speaker annotation'''
        zz = [i for i, x in enumerate(ty_list) if x == 'object']
        for ii in zz:
            oprevious_index.append(find_previous_speaker_index(s_list, ii))
            onext_index.append(find_next_speaker_index(s_list, ii))
        for (i, j, s) in zip(oprevious_index, onext_index, zz):
            object_st.append([st_list[i:j + 1], en_list[i:j + 1],
                              np.repeat(pname_list[s], len(st_list[i:j + 1]))])
        for i, z in enumerate(object_st):
            for st, en, pn in zip(z[0], z[1], z[2]):
                finalobject.append([st, en, pn])
        for i, line2 in enumerate(finalobject):
            start1 = float(line2[0])
            end1 = float(line2[1])
            p_name1 = line2[2]
            o_names[Segment(start1, end1)] = p_name1
        # print(s_names)
        N = V.shape[1]        # No of columns of for H intilisation
        F = V.shape[0]
        unique_spknames = sorted(who_speaks_when.labels())
        Km = len(unique_spknames)
        time_series = []
        n = N + 1
        nth_ele = (step * n) + ((windowSize / 2) - step)
        n = np.arange((windowSize / 2), nth_ele, step)

        for i in n:
            time_start = i - (step / 2)
            time_end = i + (step / 2)
            time_series.append([time_start, time_end])

        ''' Speaker frame id's '''

        for i, _, j in s_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            ss = np.arange(k, r + 1, 1)
            speaker_dict[j] = np.concatenate((speaker_dict[j], ss))

        ''' Addressee frame id's '''

        for i, _, j in a_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            addressee_dict[j] = np.concatenate((addressee_dict[j],
                                                s))

        ''' Object frame id's '''

        for i, _, j in o_names.itertracks(label=True):
            k = math.floor((i.start - (windowSize / 2)) / step)
            r = math.floor(((i.end - (windowSize / 2)) / step) + 1)
            s = np.arange(k, r + 1, 1)
            object_dict[j] = np.concatenate(
                (object_dict[j], s))  # object frame extraction

        '''Intersetion of adressee and object frames id's'''

        intersection = set(addressee_dict.keys()) & set(object_dict.keys())
        for key_merged in intersection:
            merged[key_merged] = np.union1d(
                addressee_dict[key_merged], object_dict[key_merged])
        unique_spknames.append('zzilence')

        '''Generate random winil and hnil'''

        h1 = np.abs(np.random.rand(Km + 1, N))
        w1 = np.abs(np.random.rand(F, Km + 1))
        frame_id = np.arange(0, N)
        scale_1 = np.sum(w1, axis=0)
        winil = np.multiply(w1, np.matlib.repmat((1 / scale_1), F, 1))
        hinil = np.multiply(h1, np.matlib.repmat(
            (scale_1[:, np.newaxis]), 1, N))
        H_st_constraint = DataArray(hinil,
                                    coords=[('pronames', unique_spknames),
                                            ('framenumber',
                                             frame_id)])
        '''Replace the speaker columns by 0 except the speaker name '''
        constrain_list = []
        for key, values in speaker_dict.items():
            for value in values:
                constrain_list.append(value)
                H_st_constraint.loc[:, value] = 0
                H_st_constraint.loc[key, value] = 1

    return (V, winil, H_st_constraint.data, unique_spknames, constrain_list, speaker_dict,
            addressee_dict,
            merged)
