import numpy as np
import matplotlib.pyplot as plt

def getPathToGroundtruth(episode):
    """Return path to groundtruth file for episode"""
    pathToGroundtruth =  "TheBigBangTheory.Season01.Episode%02d.speakername.ctm" % episode
    return pathToGroundtruth
J=getPathToGroundtruth(1)

def postagg(episode):

	pathToGroundtruth = getPathToGroundtruth(episode)

	with open(pathToGroundtruth, 'r') as f:
		for line in f:
			columns = line.strip().split()
			words   = columns[4]
			name    = columns[7]
